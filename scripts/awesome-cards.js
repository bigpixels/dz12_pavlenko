'use strict';

function Card(config) {
    var self = this;

    this.id = null;

    this._cardTitle;
    this._cardText;
    this._cardImportant;
    this._cardBlock;
    this._titleEl;
    this._textEl;
    this._importantEl;
    this._deleteButtonEl;
    this._cardsBlock;
    this._changeTitleEl;
    this._changeTextEl;
    this._changeButtonEl;

    this._createElements = function() {
        this._cardBlock = document.createElement('div');
        this._titleEl = document.createElement('h2');
        this._textEl = document.createElement('p');
        this._importantEl = document.createElement('input');
        this._deleteButtonEl = document.createElement('button');
        this._changeButtonEl = document.createElement('button');

        this._changeTitleEl = document.createElement('input');
        this._changeTextEl = document.createElement('input');
    };

    this._addElements = function() {
        this._cardBlock.appendChild(this._titleEl);
        this._cardBlock.appendChild(this._changeTitleEl);
        this._cardBlock.appendChild(this._textEl);
        this._cardBlock.appendChild(this._changeTextEl);
        this._cardBlock.appendChild(this._importantEl);
        this._cardBlock.appendChild(this._changeButtonEl);
        this._cardBlock.appendChild(this._deleteButtonEl);

        this._cardsBlock.appendChild(this._cardBlock);
    };

    this._addElementsContent = function() {
        this._cardBlock.classList.add('card-block');
        this._titleEl.textContent = this._cardTitle;
        this._textEl.textContent = this._cardText;
        this._deleteButtonEl.textContent = 'DELETE';
        this._changeButtonEl.textContent = 'CHANGE';
        this._changeButtonEl.setAttribute('data-action', 'change');

        this._importantEl.setAttribute('type', 'checkbox');
        if(this._cardImportant) {
            this._importantEl.setAttribute('checked', 'checked');
        }

        this._changeTitleEl.style.display = 'none';
        this._changeTextEl.style.display = 'none';
    };

    this._generateCard = function() {
        this._createElements();
        this._addElementsContent();
        this._addElements();
        this._saveState();
    };

    this._saveState = function() {
        if(!config.id) {
            this._generateId();
            serverData[this.id] = {
                title: this._cardTitle,
                text: this._cardText,
                important: this._cardImportant
            };
        } else {
            this.id = config.id;
        }
    };

    this._updateState = function() {
        serverData[this.id].title = this._cardTitle;
        serverData[this.id].text = this._cardText;
        serverData[this.id].important = this._cardImportant;
    };

    this._getData = function() {
        this._cardTitle = config.title;
        this._cardText = config.text;
        this._cardImportant = config.important;
    };

    this._changeCard = function() {
        this._titleEl.style.display = 'none';
        this._textEl.style.display = 'none';

        this._changeTitleEl.style.display = 'block';
        this._changeTextEl.style.display = 'block';

        this._changeTitleEl.value = this._cardTitle;
        this._changeTextEl.value = this._cardText;

        this._changeButtonEl.textContent = 'SAVE';
        this._changeButtonEl.setAttribute('data-action', 'save');
    };

    this._saveCard = function() {
        this._titleEl.textContent = this._changeTitleEl.value;
        this._textEl.textContent = this._changeTextEl.value;

        this._cardTitle = this._changeTitleEl.value;
        this._cardText = this._changeTextEl.value;

        this._changeTitleEl.style.display = 'none';
        this._changeTextEl.style.display = 'none';

        this._titleEl.style.display = 'block';
        this._textEl.style.display = 'block';

        this._changeButtonEl.textContent = 'CHANGE';
        this._changeButtonEl.setAttribute('data-action', 'change');

        this._updateState();
    };

    this._attachEvents = function() {
        this._deleteButtonEl.addEventListener('click', function (event) {
            event.preventDefault();

            self._cardsBlock.removeChild(self._cardBlock);

            delete serverData[self.id];
        });

        this._changeButtonEl.addEventListener('click', function (event) {
            event.preventDefault();
            var action = self._changeButtonEl.getAttribute('data-action');

            switch (action) {
                case 'change':
                    self._changeCard();
                    break;
                case 'save':
                    self._saveCard();
                    break;
            }
        });
    };

    this._generateId = function() {
        this.id = new Date().getTime();
    };


        this.init = function() {
            this._cardsBlock = document.querySelector('#cardsBlock');

            this._getData();
            this._generateCard();
            this._attachEvents();
        }
}
